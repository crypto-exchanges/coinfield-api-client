# CoinField API Client

Simple API client for the **CoinField** exchange.  
Target frameworks: _.NET Standard 2.0_, _.NET Core 3.1_ & .net 6.0  

[![GitLab]](https://gitlab.com/crypto-exchanges/coinfield-api-client/badges/master/pipeline.svg)
[![NuGet](https://img.shields.io/nuget/v/Alex75.CoinFieldApiClient.svg)](https://www.nuget.org/packages/Alex75.CoinFieldApiClient)  


## Functionalities

### Public
- ListPairs
- GetTicker

### Private
- GetBalance
- ListOpenOrders


## Withdrawals

Crypto: FREE
FIAT: Many options but all expensive.

2020-06-21
SEPA and Wire transfer deposit are not working: a messages says they are disabled, but only when you try to use it.

## FEE

https://www.coinfield.com/support/transfer-fees

SEPA: 0.5% with minimum cost 3 EUR
When you try to use a messaghe says "currently unavailable" !!!
WireTransfer: 1% with minimum cost 50 GBP
When you try to use it with GBP a message says is "currently unavailable" and suggest s to use USD !!!
Cards: 3/3.5%
PayPal: 4.5%  (not available on UI)

Withdraw 1000 EUR with PayPal has a fee of 45 EUR !!!!!!


## HelpDesk
2020-06-20 
Wrote to help desk about API, simple question.


## For Developers

Coinfield API docs: https://api.coinfield.com/v1/docs/    


Repository host: GitLab  
Deploy pipeline: GitLab  
NuGet host: NuGet  
