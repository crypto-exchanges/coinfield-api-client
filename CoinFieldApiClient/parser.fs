﻿module parser

open System
open System.Collections.Generic
open FSharp.Data
open Alex75.Cryptocurrencies


let parse_error jsonString =
    // not implemented yet
    // do not use "errors" (deprecated)
    // use  "alerts"
    jsonString


let parse_pairs jsonString =
    let pairs = List<CurrencyPair>()
    for item in JsonValue.Parse(jsonString).["markets"].AsArray() do
        //let id = item.["id"].Value<string>()
        let names = item.["name"].AsString().Split('/')
        pairs.Add(CurrencyPair(names.[0], names.[1]))
    pairs

let parse_ticker jsonString pair =
    let markets =  JsonValue.Parse(jsonString).["markets"].AsArray()
    if markets.Length > 1 then failwith "More then one ticker found."

    let market = markets.[0]

    let bid = market.["bid"].AsDecimal()
    let ask = market.["ask"].AsDecimal()
    let low = market.["low"].AsDecimal()
    let high = market.["high"].AsDecimal()
    let last = market.["last"].AsDecimal()

    Ticker(pair, bid, ask, Some low, Some high, Some last)
    

 (*
 {
  market:  (String - "basequote" id of the market)
  timestamp:  (String - ISO 8601 representation generated time)
  bid:  (Number - Best available bid price)
  ask:  (Number - Best available ask price)
  low:  (Number - Lowest traded price in the past 24 hours)
  high:  (Number - Highest traded price in the past 24 hours)
  last:  (Number - Last traded price)
  open:  (Number - Opening price 24 hours prior to current time)
  vol:  (Number - Total volume of the last 24 hours)
}
*)

let ParseBalance jsonString = 
    new AccountBalance(
        JsonValue.Parse(jsonString).["wallets"].AsArray()
        |> Array.map (fun item -> 
                          let currency = Currency(item.["currency"].AsString())
                          let owned = item.["balance"].AsDecimal()
                          let locked = item.["locked"].AsDecimal()
                          let available = owned - locked
                          CurrencyBalance(currency, owned, available)
                      ))

let ParseOpenOrders pair jsonString = 

    let parseOrder (pair:CurrencyPair) (item:JsonValue) = 

        let id = item.["id"].AsString()
        let type_ = match item.["strategy"].AsString() with 
                    | "market" -> OrderType.Market
                    | "limit" -> OrderType.Limit
                    | s -> failwithf "Order type (strategy) not managed: %s ." s
        let side = match item.["side"].AsString() with
                   | "bid" -> OrderSide.Buy
                   | "ask" -> OrderSide.Sell
                   | s -> failwithf "Order side not managed: %s ." s
        let openTime = item.["created_at"].AsDateTime()
        let quantity = item.["executed_volume"].AsDecimal()
        let limitPrice = if type_ = OrderType.Limit then item.["price"].AsDecimal() else 0m
        OpenOrder(id, type_, side, openTime, pair, quantity, limitPrice)

    JsonValue.Parse(jsonString).["orders"].AsArray()
    |> Array.map (parseOrder pair)

    //Array.empty<OpenOrder>