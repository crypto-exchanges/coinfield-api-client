[<NUnit.Framework.Category("Client"); NUnit.Framework.Category("REQUIRES_API_KEY")>]
module IntegrationTests.Client_private

open NUnit.Framework
open FsUnit
open Alex75.CoinFieldApiClient
open Alex75.Cryptocurrencies
open Microsoft.Extensions.Configuration

let mutable apiKey = ""

[<SetUp>]
let SetUp() =
    let conf = ConfigurationBuilder().AddUserSecrets("Coinfield.6c57cad8-4d47-4927-802e-6b9acc095996").Build()
    apiKey <- conf.["key"]

[<Test>]
let GetBalance() =
    let client = Client(apiKey) :> IApiClientPrivate
    let balance = client.GetBalance()

    balance |> should not' (be Null)
    balance |> should not' (be Empty)   // assume there is somethng


[<Test>]
let ListOpenOrders () =
    let client = Client(apiKey) :> IClient
    //let orders = client.ListOpenOrdersOfCurrencies([|CurrencyPair.XRP_ETH; CurrencyPair.BTC_USDT|])
    let orders = client.ListOpenOrdersOfCurrencies([|CurrencyPair.XRP_BTC |])
    orders |> should not' (be Null) 

