﻿module Parser.``main``

open System
open System.IO

open NUnit.Framework
open FsUnit
open Alex75.Cryptocurrencies

let solo = Currency("solo", "Sologenic")


[<Test>]
let ``parse_pairs``() =
    let pairs = parser.parse_pairs (File.ReadAllText("data/markets.json"))
    pairs |> should contain CurrencyPair.BTC_USD


[<Test>]
let ``parse_ticker`` () =
    let jsonText = File.ReadAllText("data/ticker.json")

    let ticker =  parser.parse_ticker jsonText CurrencyPair.XRP_EUR

    ticker |> should not' (be Null)
    ticker.Pair |> should equal CurrencyPair.XRP_EUR
    ticker.Ask |> should equal 0.1722
    ticker.Bid |> should equal 0.1712
    ticker.Low.Value |> should equal 0.1647
    ticker.High.Value |> should equal 0.1737
    ticker.Last.Value |> should equal 0.1716


[<Test>]
let ``ParseBalance``() =
    let jsonString = File.ReadAllText("data/wallets.json")

    let balance = parser.ParseBalance(jsonString)

    balance |> should not' (be Null)
    balance.HasCurrency(solo) |> should be True
    balance.GetCurrency(solo) |> should not' (be Null)



[<Test>]
let ``Parse OpenOrders``() =
    let jsonString = File.ReadAllText("data/open orders.json")

    let orders = parser.ParseOpenOrders (CurrencyPair("xrp", "cad")) jsonString

    orders |> should not' (be Null)
    orders.Length |> should equal 2

    orders.[0].Id |> should equal "1967728"
    orders.[1].Id |> should equal "1967673"

    orders.[0].Pair |> should equal (CurrencyPair("XRP", "CAD"))
    orders.[0].Type |> should equal OrderType.Limit
    orders.[0].OpenTime |> should (equalWithin (TimeSpan.FromSeconds(1.))) (DateTime(2018,08,21, 23,37,28))    
    orders.[0].Side |> should equal OrderSide.Buy
    orders.[0].BuyOrSellQuantity |> should equal 965.2173
    orders.[0].LimitPrice |> should equal 0.46

    orders.[1].Pair |> should equal (CurrencyPair("XRP", "CAD"))
    orders.[1].Type |> should equal OrderType.Market
    orders.[1].OpenTime |> should (equalWithin (TimeSpan.FromSeconds(1.))) (DateTime(2018,08,21, 23,37,02))    
    orders.[1].Side |> should equal OrderSide.Buy
    orders.[1].BuyOrSellQuantity |> should equal 1
    orders.[1].LimitPrice |> should equal 0

