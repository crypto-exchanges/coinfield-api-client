﻿module Flurl_utils

open System
open System.Threading.Tasks



/// return (IsSuccess, JSON content string)
let get_status_and_content (messageTask:Task<Net.Http.HttpResponseMessage>) =
    let message = messageTask.Result
    (message.IsSuccessStatusCode, message.Content.ReadAsStringAsync().Result)