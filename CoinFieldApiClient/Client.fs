﻿namespace Alex75.CoinFieldApiClient

open System
open System.Collections.Generic
open System.Threading.Tasks
open Alex75.Cryptocurrencies
open Flurl.Http


type public IClient =
    inherit IApiClient
    inherit IApiClientPrivate
    inherit IApiClientMakeOrders
    inherit IApiClientListOrders


type Client(apiKey:string) =

    let baseUrl = "https://api.coinfield.com"
    let f = sprintf
    let cache = Cache()
    let pairs_cache_time = TimeSpan.FromHours 6.0
    let ticker_cache_time = TimeSpan.FromSeconds 10.0


    let checkApiKeys () =
        if String.IsNullOrEmpty apiKey then failwithf "Private methods requires an API key"
        

    let getSymbol pair = (pair:CurrencyPair).aaabbb


    interface IClient with
      
        // public //

        member this.ListPairs(): ICollection<CurrencyPair> = 
            let url = f"%s/v1/markets" baseUrl
            match cache.GetPairs pairs_cache_time with
            | Some pairs -> pairs
            | _ -> 
                let isSuccess, jsonString = Flurl_utils.get_status_and_content( url.GetAsync())
                if not(isSuccess) then failwith (parser.parse_error jsonString)
                let pairs = parser.parse_pairs jsonString                
                cache.SetPairs pairs
                pairs :> ICollection<CurrencyPair>       

        member this.GetTicker(pair: CurrencyPair): Ticker =      

            let url = sprintf "https://api.coinfield.com/v1/tickers/%s" (getSymbol pair)
            try
                match cache.GetTicker pair ticker_cache_time with
                | Some ticker -> ticker
                | _ ->
                    let response = url.GetAsync().Result
                    let contentText = response.Content.ReadAsStringAsync().Result
                    if response.IsSuccessStatusCode then
                        let ticker = parser.parse_ticker contentText pair
                        cache.SetTicker ticker
                        ticker
                    else failwithf "Response status: %A. Response: %s" response.StatusCode contentText  //(parser.parse_error contentText)
            with exc ->
                failwithf "API call failed. %s" exc.Message


        // private //

        member this.GetBalance(): AccountBalance = 
            checkApiKeys()

            let url = "https://api.coinfield.com/v1/wallets"            
            let response = url.WithHeader("Authorization", "Bearer " + apiKey).GetAsync().Result
            let content = response.Content.ReadAsStringAsync().Result            
            parser.ParseBalance(content)

        member this.ListOpenOrdersIsAvailable = false
        member this.ListOpenOrders() =
            raise (System.NotImplementedException())

        
        member this.ListOpenOrdersOfCurrenciesIsAvailable = true
        member this.ListOpenOrdersOfCurrencies(pairs: CurrencyPair[]): OpenOrder[] = 
            checkApiKeys()
                        
            let orders = System.Collections.Concurrent.ConcurrentBag()
            let getOrders pair =                 
                let url = sprintf "https://api.coinfield.com/v1/orders/%s?limit=100&page=1&state=open&order_by=desc" (getSymbol pair)
                let response = url.WithHeader("Authorization", "Bearer " + apiKey).AllowAnyHttpStatus().GetAsync().Result
                let content = response.Content.ReadAsStringAsync().Result
                //if response.IsSuccessStatusCode
                orders.Add(parser.ParseOpenOrders pair content)

            Parallel.ForEach(pairs, (fun pair -> getOrders pair)) |> ignore
            orders.ToArray() |> Array.fold Array.append Array.empty<OpenOrder>


        member this.ListClosedOrdersIsAvailable = false
        member this.ListClosedOrders() =
            raise (System.NotImplementedException())

        member this.ListClosedOrdersOfCurrenciesIsAvailable = false
        member this.ListClosedOrdersOfCurrencies(pairs:CurrencyPair[]) =
            raise (System.NotImplementedException())

        // IApiClientMakeOrders //

        member this.CreateLimitOrder(arg1: CreateOrderRequest): string = 
            raise (System.NotImplementedException())

        member this.CreateMarketOrder(arg1: CreateOrderRequest): CreateOrderResult = 
            raise (System.NotImplementedException())        