[<NUnit.Framework.Category("Client")>]
module IntegrationTests.Client_public

open NUnit.Framework
open FsUnit
open Alex75.CoinFieldApiClient
open Alex75.Cryptocurrencies


[<Test>]
let ListPairs() =
    let client = Client(null) :> IClient
    let pairs = client.ListPairs()

    pairs |> should not' (be Empty)
    pairs |> should contain CurrencyPair.BTC_USD


[<Test>]
let GetTicker () =
    let client = Client(null) :> IApiClient

    let ticker = client.GetTicker(CurrencyPair.XRP_EUR)

    Assert.NotNull(ticker)
    Assert.IsTrue(ticker.Pair = CurrencyPair.XRP_EUR)
