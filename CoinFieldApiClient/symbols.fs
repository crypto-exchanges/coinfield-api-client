﻿module private symbols

open Alex75.Cryptocurrencies

let getPair (symbol:string) =
    let a,b = if symbol.Length = 6 then symbol.Substring(0, 3), symbol.Substring(3, 3)
                                   else symbol.Substring(0, 3), symbol.Substring(3, 4)
    CurrencyPair(a,b)